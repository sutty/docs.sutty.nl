# Configurar el entorno de trabajo con haini.sh

Usamos [Git](https://git-scm.com/) para gestionar los cambios y [haini.sh](https://0xacab.org/sutty/haini.sh) para tener un entorno de desarrollo reproducible.

Llamamos "hain" al entorno que haini\.sh reproduce.

## Configurar 0xacab

Hay que agregar llaves SSH al perfil de 0xacab.org para poder trabajar con repositorios.

```bash
cd # asegurarnos que estamos en home ~
ls .ssh/
# si no dice nada o solo dice known_hosts, necesitamos crear una llave
ssh-keygen -t ed25519
cat .ssh/id_ed25519.pub # copiar lo que sale de acá
```

Ir a <https://0xacab.org/-/profile/keys>, pegar la llave ahí y guardar.

## Entorno de trabajo

Cosas generales que podemos ir incorporando/preparando:

* Vamos a usar mucha terminal, elijamos una que nos resulte cómoda de leer.  Por lo general fondos oscuros cansan menos la vista, muchas distribuciones configuran sus terminales con transparencia, se puede cambiar en sus opciones.  También una tipografía con alto que nos resulte cómodo ayuda un montón.

* Prompt: La información que muestra la terminal a la izquierda de donde podemos escribir comandos se llama "prompt".  Por defecto suele mostrar el directorio actual.  Es importante prestarle atención porque es un indicador de dónde estamos parades.

    En los ejemplos no lo ponemos porque cada prompt puede ser diferente, uno con información mínima y útil podría ser:

    ```bash
    echo 'export PS1="\$(git rev-parse --abbrev-ref HEAD) \W >_ "' >> ~/.bashrc
    source ~/.bashrc
    # Cambia el prompt a: rama_de_git nombre_del_directorio >_ a partir de acá podemos escribir
    # Por ejemplo:
    rails sutty >_ make serve
    ```

    Tené en cuenta que esta prompt no va a estar en la terminal dentro de hain.

* Editor de texto.  El que queramos, con resaltado de sintaxis, indentación (sangría!) automática y un esquema de colores que nos resulte cómodo.

    ```bash
    # La variable de entorno EDITOR contiene nuestro editor por defecto, si la queremos cambiar, agregamos al final de estos archivos, son `>>`, cuidado!! `>` pisa todo el contenido (a quién se le ocurren estas cosas.)
    echo 'export EDITOR=vim' >> ~/.bashrc # Cambiar vim por lo que corresponda
    echo 'export EDITOR=vim' >> ~/.bash_profile
    # Hace falta reiniciar la terminal o hacer esto para que tome efecto inmediato
    source ~/.bashrc
    ```

    Tené en cuenta que esta prompt no va a estar en la terminal dentro de hain.

* Herramientas: Instalar y configurar git y git-lfs.

    ```
    # Crear el archivo ~/Project/Sutty/.gitconfig con la configuración específica para Sutty
    [user]
    name = NOMBRE
    email = MAIL@sutty.nl

    # Crear o editar ~/.gitconfig con la configuración general
    [init]
    defaultBranch = main

    [core]
    quotepath = off

    [color]
    ui = true

    [push]
    default = simple

    [pull]
    default = simple
    rebase = false

    [merge]
    tool = vimdiff

    [mergetool]
    keepBackup = false

    [rebase]
    autosquash = true

    [filter "lfs"]
    process = git-lfs filter-process
    required = true
    clean = git-lfs clean -- %f
    smudge = git-lfs smudge -- %f

    [includeIf "gitdir:~/Projects/Sutty"]
    path = ~/Project/Sutty/.gitconfig
    ```

* Paciencia :P

* A veces da fiaca pero para practicarlo podemos tomar notas colectivas en pads que nos vayan quedando como documentación para ir emprolijando.

* Directorio de trabajo.  Para no tener los proyectos flotando por todos lados, podemos tener un directorio/carpeta de trabajo para Sutty donde coloquemos todos los proyectos, por ejemplo:

```bash
# Asegurarnos que estamos en nuestro directorio de origen ("home")
cd ~
# Crear el directorio de trabajo
mkdir -p Projects/Sutty
```

A partir de este momento, todos los comandos que corramos en esta guía a menos que se indique lo contrario implican que se abrió la terminal y se cambió la posición a este directorio, con:

```bash
cd Projects/Sutty
```

Puede ser el nombre que queramos pero todo tendría que estar dentro de un directorio para que no lo perdamos y no tengamos que recordar donde estaba cada cosa cuando la queramos ir a buscar.

> Nota: los próximos comandos son ejemplos y no van a funcionar ya que no tenemos los repositorios clonados todavía.

Para poder trabajar con comodidad y no estar pensando dónde estamos parades en cada comando (o correrlos donde no corresponda y obtener errores inesperados que nos hagan perder el tiempo buscándolos), la recomendación es trabajar siempre desde el directorio de origen/raíz de cada proyecto.  Si vamos a trabajar en sutty, lo primero que hacemos es:

```bash
cd ~/Projects/Sutty/sutty
```

Y a partir de ese momento no volvemos a usar `cd` para movernos a otro lado.  Si queremos abrir un archivo, en lugar de movernos al directorio y después abrirlo, podemos usar su ubicación relativa a la raíz:

```bash
vim app/controllers/post_controller.rb
```

Al cerrar el editor estaremos en el mismo directorio.

Si queremos movernos rápido, podemos hacer:

```bash
cd app/controllers/
...
cd -
```

El `-` en `cd` indica el directorio anterior, podemos ir y venir entre uno y otro rápidamente así.

Usando ubicaciones relativas podemos abrir archivos de otros proyectos también:

```bash
vim ../sutty-base-jekyll-theme/_layouts/default.html
```

## Haini\.sh

Tuvimos muchos problemas instalando dependencias de Sutty en las computadoras de cada desarrolladorx, entonces creamos esta herramienta para simplificarlo.

Para bajarla:

```bash
# Clonar el repositorio dentro de Sutty
cd ~/Projects/Sutty
# Es importante que sea ahí! Los otros repositorios asumen que está en `../haini.sh`
git clone git@0xacab.org:sutty/haini.sh.git
```

Los distintos repositorios van a usar haini.sh automáticamente, y la primera vez que lo uses se va a generar el entorno automáticamente.

### Importar certificados

Por más que haini.sh importa los certificados que genera al sistema ya sea dentro y afuera de haini.sh, Firefox y Chrome no toman los certificados del sistema y usan su propia lista. Hay que importarlos manualmente, así es en Firefox:

1. Ir a <about:preferences#privacy> y apretar en "Ver certificados..."
    
    ![Captura de pantalla de la sección de certificados](configurar-entorno/screenshot-certificados.png)
2. Ir a la pestaña de "Autoridades" y apretar en "Importar..."
    
    ![Captura de pantalla de la pestaña de autoridades](configurar-entorno/screenshot-autoridades.png)
3. Seleccionar el certificado en `Sutty/hain/usr/local/share/ca-certificates/ca-sutty.crt`


## Sutty

Estas instrucciones son específicas para el desarrollo del _panel_ Sutty y no para otros componentes.

### Base de datos

Al principio usábamos bases de datos SQLite3, pero ahora necesitamos características específicas de PosgreSQL (como el buscador).  El servidor de PG corre dentro de Hain, con lo que no tenemos que hacer nada más.

### Configuración inicial

Solo hay que hacerla una vez!

```bash
cd ~/Projects/Sutty
# Clonar el repositorio de git de Sutty
git clone git@0xacab.org:sutty/sutty.git

# Esto crea el directorio/carpeta sutty/ desde donde vamos a trabajar a partir de ahora.  Ya no es necesario clonar pero sí estar dentro de este directorio para trabajar.
cd sutty

# O directamente al iniciar la terminal
cd ~/Projects/Sutty/sutty

# Configuramos las variables de entorno.  El archivo .env no se comitea (está ignorado por git), porque sino nos estaríamos intercambiando configuraciones específicas de cada quién.
cp .env.example .env
$EDITOR .env # Editar el archivo con nuestro editor preferido

# Muchas de estas variables de entorno tienen valores por defecto y no hace falta adaptarlas:

SUTTY=sutty.local # el dominio principal de sutty, esta ubicación es propia de nuestra computadora y no puede visitarse desde afuera, está bien para trabajar individualmente
SUTTY_WITH_PORT=sutty.local:3000 # Si cambiamos SUTTY hay que cambiar esta variable también
TIENDA=tienda.sutty.local # Idem

# Generamos los secretos
make rails args="credentials:edit"

# Este paso va a abrir un editor, sobre el que vamos a agregar estas líneas, guardar y salir:
secret_key_base: un texto largo que rails ya genera por nosotres
devise_pepper: puede ser el mismo que secret_key_base
lockbox_master_key: tiene que ser de 64 caracteres hexadecimales, por ejemplo con `openssl rand -hex 32`
airbrake: misma longitud que secret_key_base

# Al guardar y salir, se genera un archivo config/credentials.yml.enc y config/master.key, no son los usados en produccion y no es necesario agregarlos a git.

# Iniciar la base de datos
make postgresql

# Configuramos la base de datos.  Este paso genera la base de datos y le sube los datos iniciales.  Para más datos sobre la base de datos, la configuración está en config/database.yml
make rake args="db:prepare"

# Si se queja que faltan migraciones, correrlas con
make rake args="db:migrate"

# Llenar la base de datos con una semilla. Concretamente, esto crea pseudo-sitios necesarios para el funcionamiento correcto del panel.
make rake args="db:seed"
```

### Iniciar el entorno de trabajo

Para poder desarrollar Sutty y hacer pruebas en vivo, iniciamos el servidor de desarrollo:

```bash
make serve
# Se puede cerrar con control+C, también conocido como ^C
```

A partir de ahí podemos abrirlo en el navegador de trabajo con la dirección <https://panel.sutty.local:3000/>

Cuando hagamos cambios en los archivos, Rails los detecta y recarga la información.  Con esto podemos desarrollar haciendo pruebas en vivo.


## Tienda

TODO: escribir comandos, cambiar si cambia [haini.sh#23](https://0xacab.org/sutty/haini.sh/-/issues/23)

Es necesario crear una base de datos `tienda` con usuarix `tienda` sin contraseña, antes del paso `bundle exec rake db:setup`.
