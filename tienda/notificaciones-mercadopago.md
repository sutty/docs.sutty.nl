---
title: Configurar las notificaciones de pago de MercadoPago
description: Las notificaciones de pago automatizan el procesamiento de pagos
---

Para que la tienda Sutty pueda procesar automáticamente los pagos realizados a través de MercadoPago, hay que configurar su servicio de notificaciones instantáneas, IPN.

1. Ingresar a <https://www.mercadopago.com.ar/developers/panel/notifications/ipn> y completar con los datos
2. **URL del sitio web en producción:** La dirección de tu tienda Sutty
3. **Eventos:** Pagos
4. **Guardar! No es necesario hacer la prueba de conexión.**
