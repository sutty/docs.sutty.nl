---
title: Configurar MercadoPago en la tienda
description: Para poder procesar pagos desde la tienda Sutty en MercadoPago, es necesario obtener las credenciales de acceso
---

Para poder generar solicitudes de pago en MercadoPago, es necesario obtener las credenciales de acceso.  Estas credenciales de acceso necesitan guardarse de forma segura.  En Sutty tomamos los recaudos para que esto no suceda, pero recomendamos no enviárnosla por correo.  En esta guía van a poder obtener las credenciales y configurarlas directamente en la tienda Sutty sin compartirla a través de otros medios.

1. Ingresar a <https://www.mercadopago.com.ar/developers/panel/credentials>
2. Obtener las credenciales de producción
3. Sin cerrar esta ventana, abrir la tienda en otra.  Desde <https://panel.sutty.nl> pueden usar el botón "Tienda" en la esquina superior derecha.
4. En el menú izquierdo, abrir **Configuraciones & Medios de pago**
5. En la lista, buscar Mercado Pago o cualquier ítem con el nombre Spree::PaymentMethod::MercadoPago y presionar el ícono de lápiz para abrirlo
6. Si no existe, lo podemos crear, con el botón "Agregar forma de pago" en la esquina superior derecha
7. En Proveedore, debe decir Spree::PaymentMethod::MercadoPago
8. En Tienda, seleccionan la tienda (por lo general hay una, ¡pero puede haber varias!)
9. En Mostrar, seleccionan "Ambos"
10. En Nombre, "Mercado Pago" o lo que prefieran
11. La Descripción es opcional, pueden usarla para comunicar algo a sus clientxs
12. Si están creando el medio de pago, presionan Crear para guardar los cambios y recargar la página
13. En Public Key copian y pegan la Public Key de Mercado Pago que obtuvieron en el paso 2
14. En Access Token, copian y pegan el de Mercado Pago como en el paso anterior
15. En Back URL Success, la dirección de la página de confirmación, por ejemplo: `https://tienda.sutty.nl/confirmacion/`. Esta es la dirección donde vuelven lxs clientxs al terminar el pago
16. En Back URL Failure, la dirección de la página del carrito, por ejemplo: `https://tienda.sutty.nl/carrito/`. Aquí vuelven lxs clientxs cuando no pudieron pagar
17. Auto Return: approved (ya lo dice, así que no lo tienen que modificar)
18. Deshabilitar el modo de prueba
19. Guardar los cambios con el botón Actualizar
