Crear una tienda
================

Las tiendas se crean desde el servidor tienda.sutty.nl.

Cada tienda tiene su propia copia de la base de datos.

- Nombre: El nombre de la tienda legible por humanes (ej: "La Periférica")
- nombre-publico: El nombre en minúsculas, sin tildes y con guiones en lugar de espacios ("la-periferica"), es el subdominio
- nombre_interno: Igual para con guiones bajos ("la_periferica"), es el nombre de la base de datos


```bash
# Entrar al servidor de la tienda
ssh root@tienda.sutty.nl
# Entrar al contenedor que expide y renueva certificados
docker exec -it certbot /bin/sh
# Agregar el dominio a la variable de entorno
export DOMAINS="$DOMAINS nombre-publico.tienda.sutty.nl"
# Expedir los certificados
/usr/local/bin/certbot
# Salir del contenedor
exit
# Entrar al contenedor de la tienda
docker exec -it tienda /bin/sh
su app
cd
bundle exec rails c
# Crear la tienda, vamos a recibir un correo con instrucciones de confirmación
TiendaService.create nombre: 'Nombre', admin: 'email', logo: 'url del logo'
exit
# Reiniciar la tienda para que tome la tienda nueva
cat tmp/puma.pid | xargs -r kill -USR2
# Volver a entrar a la consola de la tienda
bundle exec rails c
# Cambiar al entorno de la tienda, cada vez que queramos trabajar con una tienda hay que hacer esto
Apartment::Tenant.switch! 'valor de database de la tienda'
# Una vez que la cuenta esté confirmada generamos y obtenemos la api key
Spree::User.first.generate_spree_api_key!
Spree::User.first.spree_api_key
exit
```

Copiar la API key y colocarla en el sitio de Sutty.

```bash
ssh root@athshe.sutty.nl
docker exec -it sutty /bin/sh
su app
cd
bundle exec rails c
site = Site.find_by_name 'nombre-publico'
site.update tienda_api_key: "la api key de la tienda", tienda_url: "no cambiar a menos que no coincida el nombre-publico en sutty con la tienda"

# Generar el sitio sin notificar a les usuaries
DeployJob.perform_now site.id, false
```

## MercadoPago

[Obtener las credenciales de MercadoPago](https://docs.sutty.nl/tienda/2021/06/22/obtener-las-credenciales-de-mercadopago.html)


## Migrar una editorial autogestiva a tienda

* Hacer todos los pasos para gestionar la tienda
* Copiar el archivo `_plugins/migration.rb` desde sudakuir para poder agregarle SKUs a todos los productos al repositorio del sitio nuevo.
* Desde el panel crear un artículo para cada proceso de pago
* Generar la tienda


## Configuración inicial

TODO: Automatizar desde TiendaService

Ir a configuración

### Preferencias generales

* Comprobar que esté en ARS
* Sacar inglés y agregar español
* Guardar

### Tiendas

* Comprobar que existe la tienda con la URL del sitio público
* Comprobar que la moneda sea ARS

### Categorías impositivas

Está bien que esté vacío acá

### Tasas de cambio

Si la tienda va a vender en otras monedas, gestionar las tasas de cambio acá.

### Tasas impositivas

También puede quedar vacío

### Zonas

Las zonas agrupan países o estados donde aplican distintos medios de envío

* Crear una zona "A todo el mundo"
* Basada en país
* Volver a la consola de Rails:

```ruby
Apartment::Tenant.switch! 'valor de database'
z = Spree::Zone.first
Spree::Country.find_each do |c|
  z.zone_members.create zoneable: c
end
```

### Países

Ya están todos (?)

### Estados

TODO: Cargar AMBA

Podemos cargar ciudades y partidos acá, por ejemplo AMBA

### Métodos de pago

#### Mercado Pago

TODO: Crear página de fallo y pending

Elegir MercadoPago en la lista desplegable.  Completar con nombre.  Al guardar va a pedir las credenciales que se obtiene de acá:

https://www.mercadopago.com.ar/developers/panel/credentials

En success_url: https://la-tienda.sutty.nl/confirmacion/

## Métodos de envío

Nombre: A combinar
Mostrar: Ambos
Categoría de envío: default
Zona: Todo el mundo
Calculadora: Tarifa plana por pedido (al guardar dejar en 0)

## Ubicaciones de stock

Comprobar que exista Depósito

## Transferencias de stock

No hay que hacer nada















