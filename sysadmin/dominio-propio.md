Configurar un dominio para apuntarlo a Sutty
============================================

* Apuntar el dominio a Sutty

    Forma simple:
    
    * Eliminar todos los registros A y AAAA del DNS para el dominio que apunte a Sutty y su www (ej: `osmiornicabiblioteca.com` y `www.osmiornicabiblioteca.com`)
    
    * Crear un registro CNAME para el dominio que apunte a Sutty: `www.osmiornicabiblioteca.com. CNAME sutty.nl.` (¡Los puntos al final son importantes!)
        
        | TIPO | NOMBRE | OBJETIVO |
        | -    |   -    |    -     |
        | CNAME| www    | osmiornicabiblioteca.com.|
        | CNAME| @      | sutty.nl.|

* Probar que funcione: `drill osmiornicabiblioteca.com.` debería responder con las IPs de Sutty.

* Configurar el sitio en Sutty:
    
    ```bash
    # Conectarse al servidor
    ssh root@athshe.sutty.nl
    # Entrar en el contenedor de Sutty
    # Tip: con `tmux attach` siempre hay terminales abiertas
    docker exec -it sutty /bin/sh
    # Convertirse en lx usuarix de la app
    su app # No usar -
    # Ir al home
    cd
    # Entrar en la consola de Rails
    bundle exec rails console
    # Encontrar el sitio por el nombre que le pusimos al panel
    site = Site.find_by_name('osmiornicabiblioteca')
    # Agregar los dominios alternativos
    site.deploys.create(type: 'DeployAlternativeDomain', hostname: 'osmiornicabiblioteca.com.')
    site.deploys.create(type: 'DeployAlternativeDomain', hostname: 'www.osmiornicabiblioteca.com.')
    # Hacer un deploy de prueba, el segundo argumento es para no notificar
    DeployJob.perform_now(site.id, false)
    ```
    
    Desde el momento en que creamos los `DeployAlternativeDomain` puede pasar un minuto hasta que Sutty genera los certificados.


## Por qué el punto al final es importante

El DNS es como un sistema de archivos de información, solo que se lee al revés.  Las computadoras interpretan `www.osmiornicabiblioteca.com` como `/com/osmiornicabiblioteca/www`, o sea invirtiendo la dirección del árbol y usando puntos en lugar de barras.  El `.` al final representa la raíz es decir la primera `/`.  Muchos programas lo aplican tácitamente, pero dentro de DNS es necesario, de otra forma interpreta el subdominio como una parte de una carpeta, entonces `www` sin punto al final es `/com/osminiornicabiblioteca/www` pero `www.` es `/www`.

Si no usamos `sutty.nl.` en el `CNAME`, corremos el riesgo de configurar `sutty.nl.osmiornicabiblioteca.com` :B


## Errores

### Secure Connection Failed

Este error significa que Sutty todavía no sacó los certificados, si pasa más de un minuto desde que se crea el dominio alternativo puede ser que la configuración del DNS no sea correcta.

```
An error occurred during a connection to www.osmiornicabiblioteca.com. Peer reports it experienced an internal error.

Error code: SSL_ERROR_INTERNAL_ERROR_ALERT
```
